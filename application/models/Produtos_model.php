<?php 
include_once APPPATH. 'libraries/Pedido.php';
class Produtos_model extends CI_Model{

   public $id;
   public $categoria;
   public $titulo;
   public $detalhes;
   public $img;
   public $preco;
   public $quantidade;
   public $total;
   public $usuario;

   public function __construct(){
      parent::__construct();
   }

   public function listar_produtos(){
        $this->db->select('produtos.id, produtos.categoria, produtos.titulo, produtos.img, 
        produtos.preco, produtos.quantidade');
        $this->db->from('produtos');
        $this->db->limit(12);
        return $this->db->get()->result();
   }

   public function categoria_produtos($id, $pular, $prod_por_pagina){
        $this->db->select('produtos.id, produtos.categoria, produtos.titulo, produtos.detalhes, produtos.img, 
        produtos.preco, produtos.quantidade');
        $this->db->from('produtos');
        $this->db->where('produtos.categoria ='.$id);
        if($pular && $prod_por_pagina){

          $this->db->limit($prod_por_pagina,$pular);
          
        }else{
          $this->db->limit(4);
        }
        return $this->db->get()->result();
   }

   public function produto($id){
          $this->db->select('produtos.id, produtos.categoria, produtos.titulo, produtos.detalhes, produtos.img, 
          produtos.preco, produtos.quantidade');
          $this->db->from('produtos');
          $this->db->where('produtos.id ='.$id);
          return $this->db->get()->result();
   }

   public function listar_titulo($id){
          $this->db->select('id, titulo');
          $this->db->from('produto');
          $this->db->where('id ='.$id);
          return $this->db->get()->result();
   }

   public function listar_produto($pular=null,$prod_por_pagina=null){

      //lista produtos no painel admin.
      if($pular && $prod_por_pagina){

        $this->db->limit($prod_por_pagina,$pular);
        
      }else{
        $this->db->limit(5);
      }

      return $this->db->get('produtos')->result();
   }

   public function listar_produtoss($id){
      //lista somente um produto
      $this->db->where('md5(id)', $id);
      return $this->db->get('produtos')->result();
   }

   public function adicionar($id, $titulo, $detalhes, $preco, $categoria){
     $dados['id'] = $id;
     $dados['titulo'] = $titulo;
     $dados['detalhes'] = $detalhes;
     $dados['preco'] = $preco;
     $dados['categoria'] = $categoria;
     return $this->db->insert('produtos', $dados);
   }
 
   public function excluir($id){
    $this->db->where('md5(id)', $id);
    return $this->db->delete('produtos');
   }

   public function alterar($titulo, $detalhes, $preco, $categoria, $id){
    $dados['titulo'] = $titulo;
    $dados['detalhes'] = $detalhes;
    $dados['preco'] = $preco;
    $dados['categoria'] = $categoria;
    $this->db->where('id', $id);
    return $this->db->update('produtos', $dados);
   }

   public function alterar_img($id){
    $dados['img'] = 1;
    $this->db->where('md5(id)', $id);
    return $this->db->update('produtos', $dados);
   }

   public function contar(){
      //paginação painel admin.
      return $this->db->count_all('produtos');
   }
    
   public function contar_frontend($id){
      $this->db->where('categoria ='.$id);
      return $this->db->count_all_results('produtos');
   }

   public function adicionar_pedido($usuario, $email, $telefone, $titulo, $detalhes, $quantidade, $preco, $total, $data){
      $dados['userlogado'] = $usuario;
      $dados['email_user'] = $email;
      $dados['telefone_user'] = $telefone;
      $dados['titulo_prod'] = $titulo;
      $dados['detalhes_prod'] = $detalhes;
      $dados['quantidade_prod'] = $quantidade;
      $dados['preco_prod'] = $preco;
      $dados['total'] = $total;
      $dados['data_pedido'] = $data;
      return $this->db->insert('pedido', $dados);
   }

   public function gera_pedido(){
      $this->db->select('id, userlogado, email_user, telefone_user, titulo_prod, detalhes_prod, quantidade_prod, preco_prod, total, data_pedido, ativo');
      $this->db->from('pedido');
      return $this->db->get()->result();
   }
 
   public function statusPedido($status=NULL, $id=NULL){
      if ($status != NULL && $id != NULL):
          $this->db->update('pedido', $status, array('id'=>$id));            
      endif;
   } 

   public function pedido_ativo(){
      $this->db->select('userlogado, titulo_prod, detalhes_prod, quantidade_prod, preco_prod, total, ativo');
      $this->db->where('ativo' == 1);
      $this->db->from('pedido');
      return $this->db->get()->result();
   }
   
   public function getProdutoByID($id=NULL){
      if ($id != NULL):
         $this->db->where('id', $id);          
         $this->db->limit(1);
         $query = $this->db->get("pedido");        
         return $query->row();   
      endif;
   } 

   public function gera_pedido_cliente(){
      $this->db->select('userlogado, titulo_prod, detalhes_prod, quantidade_prod, preco_prod, total, ativo');
      $this->db->where('userlogado', $this->session->userdata('userlogado')->nome);
      $this->db->from('pedido');
      return $this->db->get()->result();
   }


   public function vendas(){
 
      date_default_timezone_set ('America/Bahia');
      $ano = date ('Y');
      $sql = "SELECT YEAR(data_pedido) AS ano, titulo_prod, SUM(quantidade_prod) AS quantidade_prod, MONTH (data_pedido) AS mes FROM pedido WHERE YEAR(data_pedido) = $ano AND MONTH(data_pedido) = MONTH(data_pedido) GROUP BY mes, titulo_prod ORDER BY quantidade_prod desc";
      $resultado = $this->db->query($sql);
      return  $resultado->result();
   }

   public function deletar($id){
      $this->db->where('id', $id);
      return $this->db->delete('pedido');
   }

   public function buscar($pular, $prod_por_pagina){
      
      $busca = $this->input->post('busca');
      $this->db->select('*');
      $this->db->like('titulo', $busca);
      $this->db->limit( $prod_por_pagina, $pular);
      return $this->db->get('produtos')->result();
   }
   
}