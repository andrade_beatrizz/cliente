<?php

include_once APPPATH. 'libraries/Avaliacao.php';

class Avaliacao_model extends CI_Model{

    public $id;

    public function gera_tabela_av($id){
        $this->db->select('id, id_produto, user, comentario, data');
        $this->db->where('id_produto ='.$id);
        $this->db->from('avaliacao_produto');
        return $this->db->get()->result();
    }

    public function salva_avaliacao(){

        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('avaliacao_produto[comentario]', 'Comentário', 'trim|required|min_length[2]|max_length[255]');
            if($this->form_validation->run()){
                //Passou na validação
                //executar a acao do formulario
                $data = $this->input->post('avaliacao_produto');
                $id = $data['id_produto'];
                $nome = $data['nome'];
                $avaliacao = new Avaliacao();
                $avaliacao->gera_avaliacao($data);

                redirect(base_url('index.php/produto/'.$id.'/'.$nome.'?avaliacao=1'));
            }
    }

    public function adicionar_avaliacao($id, $usuario, $comentario){
        $dados['id_produto'] = $id;
        $dados['user'] = $usuario;
        $dados['comentario'] = $comentario;
        return $this->db->insert('avaliacao_produto', $dados);
    }

    public function deletar($id){
        $this->db->where('id', $id);
        return $this->db->delete('avaliacao_produto');
    }

}