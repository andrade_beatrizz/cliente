<?php

include_once APPPATH. 'libraries/Msg.php';

class Msg_model extends CI_Model{

    public $id;

    public function gera_tabela(){
        $this->db->select('id, nome, email, telefone, mensagem');
        $this->db->from('mensagens');
        return $this->db->get()->result();
    }

    public function salva_mensagem(){
        if(sizeof($_POST) == 0) return;

        $this->form_validation->set_rules('mensagens[nome]', 'Nome', 'trim|required|min_length[2]|max_length[20]');
        $this->form_validation->set_rules('mensagens[email]', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('mensagens[telefone]', 'Telefone', 'trim|required|min_length[10]|max_length[11]');
        $this->form_validation->set_rules('mensagens[mensagem]', 'Mensagem', 'trim|required|min_length[2]|max_length[255]');
            if($this->form_validation->run()){

                $data = $this->input->post('mensagens');
                $msg = new Msg();
                $msg->gera_mensagem($data);

                redirect(base_url('index.php/contato?contato=1'));
            }
    }

    public function deletar($id){
        $this->db->where('id', $id);
        return $this->db->delete('mensagens');
    }

}