<?php 

class Categorias extends CI_Controller{

   public function __construct(){
      parent::__construct();
      $this->load->model('categorias_model', 'ModelCategorias');
      $this->categorias = $this->ModelCategorias->listar_categorias();
   }

   public function index($id, $titulo, $pular=null, $prod_por_pagina=null){
      
        $this->load->model('produtos_model', 'ModelProdutos');
        $this->load->library('pagination');

        $config['base_url'] = base_url("index.php/categoria/".$id.'/'.$titulo );
        $config['total_rows'] = $this->ModelProdutos->contar_frontend($id);
        $prod_por_pagina = 4;
        $config['per_page'] = $prod_por_pagina; 
        $this->pagination->initialize($config);

        $dados['links_paginacao'] = $this->pagination->create_links();
        $this->load->helper('funcoes');
        $dados['categorias'] =  $this->categorias;
        $dados['produtos'] = $this->ModelProdutos->categoria_produtos($id, $pular, $prod_por_pagina);

        //dados a serem enviados para o cabeçalho
        $dados['titulo'] = 'Categorias';
        $dados['subtitulo'] = '';
        $dados['subtitulodb'] = $this->ModelCategorias->listar_titulo($id);

        $this->load->view('common/header', $dados);
        $this->load->view('common/navbar');
        $this->load->view('frontend/categoria');
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
	}
    
}