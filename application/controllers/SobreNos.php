<?php 

class SobreNos extends CI_Controller{

   public function __construct(){
      parent::__construct();
      $this->load->model('categorias_model', 'ModelCategorias');
      $this->categorias = $this->ModelCategorias->listar_categorias();
   }

   public function index(){
    $this->load->helper('funcoes');
    $dados['categorias'] =  $this->categorias;

    //dados a serem enviados para o cabeçalho
    $dados['titulo'] = 'Ateliê Annye Santos';
    $dados['subtitulo'] = 'Sobre Nós';
    $this->load->view('common/header', $dados);
    $this->load->view('common/navbar');
    $this->load->view('frontend/blog');
    $this->load->view('common/rodape');
    $this->load->view('common/footer');
   } 
   
  
   
}
