<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    public function __construct(){
        parent::__construct();
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo > 1){
            redirect(base_url('index.php/admin'));
        }
        $this->load->model('categorias_model', 'ModelCategorias');
        $this->categorias = $this->ModelCategorias->listar_categorias();
 
    }
    

	public function index(){

        $this->load->helper('funcoes');
        $dados['categorias'] =  $this->categorias;
        $this->load->model('produtos_model', 'ModelProdutos');
        $dados['produtos'] = $this->ModelProdutos->listar_produtos();

        //dados cabeçalho
        $dados['titulo'] = 'Área do usuário';
        $dados['subtitulo'] = $this->session->userdata('userlogado')->nome;
        
        $this->load->model('Produtos_model', 'ModelProdutos');
        $this->load->view('common/header', $dados);
        $this->load->view('common/navbar');
        
        $dados['usuario'] = $this->session->userdata('userlogado');
        $dados['pedido'] = $this->ModelProdutos->gera_pedido_cliente();
        $this->load->view('frontend/meus_pedidos', $dados);
    
        $this->load->view('common/rodape');
        $this->load->view('common/footer');
		
    }
    

}