<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//Essa é a página home!
class Ans extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('categorias_model', 'ModelCategorias');
        $this->categorias = $this->ModelCategorias->listar_categorias();
    }

	public function index(){

        $this->load->helper('funcoes');
        $dados['categorias'] =  $this->categorias;
        $this->load->model('produtos_model', 'ModelProdutos');
        $dados['produtos'] = $this->ModelProdutos->listar_produtos();

        //dados a serem enviados para o cabeçalho
        $dados['titulo'] = 'Página Inicial';
        $dados['subtitulo'] = 'Ateliê Annye Santos';

        $this->load->view('common/header', $dados);
        $this->load->view('common/navbar');
       
        $this->load->view('frontend/home/carousel');
        $this->load->view('frontend/home/about/cards');
        $this->load->view('frontend/home/jumbotron');

        $this->load->view('common/rodape');
        $this->load->view('common/footer');
		
	}

}