<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->model('categorias_model', 'ModelCategorias');
        $this->categorias = $this->ModelCategorias->listar_categorias();
    }

    public function index(){
        
        $this->load->helper('funcoes');
        $dados['categorias'] =  $this->categorias;

        //dados a serem enviados para o cabeçalho
        $dados['titulo'] = 'Ateliê Annye Santos';
        $dados['subtitulo'] = 'Contato';
        

        $this->load->view('common/header', $dados);
        $this->load->view('common/navbar');
       
        $this->load->model('Msg_model', 'ModelMsg');
        $this->ModelMsg->salva_mensagem();
        $this->load->view('frontend/contato');
        if(isset($_GET['contato']) && $_GET['contato']){
            echo "<script>alert('Sua mensagem foi enviada! Obrigada pela participação.');</script>";
         }

        $this->load->view('common/rodape');
        $this->load->view('common/footer');
    }


}