<?php 

class Produto extends CI_Controller{

   public function __construct(){
      parent::__construct();
      $this->load->model('categorias_model', 'ModelCategorias');
      $this->categorias = $this->ModelCategorias->listar_categorias();
      $this->load->model('produtos_model', 'ModelProdutos');
   }

   public function index($id){

      $this->load->helper('funcoes');
      $dados['categorias'] =  $this->categorias;
      $dados['produtos'] =  $this->ModelProdutos->produto($id);
      
      //dados a serem enviados para o cabeçalho
      $dados['titulo'] = 'Produto';
      $dados['subtitulo'] = 'Detalhes';    
      $dados['usuario'] = $this->session->userdata('userlogado');

      $this->load->view('common/header', $dados);
      $this->load->view('common/navbar');

      $this->load->view('frontend/detalhe', $dados);
         if(isset($_GET['pedido']) && $_GET['pedido']){
            echo "<script>alert('Seu pedido foi realizado! Entraremos em contato o mais breve possível. TOTAL DO PEDIDO = R$ .$total');</script>";
         }
      $this->load->model('Avaliacao_model', 'ModelAvaliacao');
      $this->ModelAvaliacao->salva_avaliacao();
      $this->load->view('frontend/avaliacao', $dados);
      $dados['avaliacao_produto'] = $this->ModelAvaliacao->gera_tabela_av($id);
      $this->load->view('frontend/avaliacoes', $dados);
         if(isset($_GET['avaliacao']) && $_GET['avaliacao']){
            echo "<script>alert('Sua avaliação foi registrada e logo você poderá conferí-la! Obrigada pela participação.');</script>";
         }

      $this->load->view('common/rodape');
      $this->load->view('common/footer');

   } 

   public function add_produto_escolhido(){
      
      if(!$this->session->userdata('logado')){
         redirect(base_url('index.php/admin/login'));
      }
      if($this->session->userdata('userlogado')->tipo > 1){
         redirect(base_url('index.php'));
      }
      $id = $this->input->post('id');
      $nome = $this->input->post('nome');
      $usuario = $this->input->post('userlogado');
      $email = $this->input->post('email_user');
      $telefone = $this->input->post('telefone_user');
      $titulo= $this->input->post('titulo_prod');
      $detalhes= $this->input->post('detalhes_prod');
      $quantidade= $this->input->post('quantidade'); 
      $preco= $this->input->post('preco_prod'); 
      $total= $quantidade * $preco; 
      $this->ModelProdutos->adicionar_pedido($usuario, $email, $telefone, $titulo, $detalhes, $quantidade, $preco, $total);
      $this->session->set_flashdata('categorias-ok',
      '<div class="alert alert-success">Pedido realizado com sucesso!</div>');
      redirect(base_url('index.php/login'));


   }

   public function deletar($id){

      $this->load->model('Avaliacao_model', 'ModelAvaliacao');
      $this->ModelAvaliacao->deletar($id);
      $voltar = "<script>window.open(document.referrer,'_self');</script>";
      echo $voltar;
         
  }

}


  

