<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ans extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //verificação de login
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
    }

	public function index(){
        //dados cabeçalho
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Home';

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        $this->load->view('backend/home');
        $this->load->view('backend/template/footer');
		
	}


}