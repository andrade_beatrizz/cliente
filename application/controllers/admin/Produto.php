<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Controller {

    public function __construct(){
        parent::__construct();

        //verificação login
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
        $this->load->model('produtos_model', 'ModelProdutos');
        $this->load->model('categorias_model', 'ModelCategorias');
        $this->categorias = $this->ModelCategorias->listar_categorias();
    }

    public function index($pular=null, $prod_por_pagina=null){

        $this->load->helper('funcoes');
        $this->load->library('table');

        //paginação
        $this->load->library('pagination');

        $config['base_url'] = base_url("index.php/admin/produto");
        $config['total_rows'] = $this->ModelProdutos->contar();
        $prod_por_pagina = 5;
        $config['per_page'] = $prod_por_pagina;

        $this->pagination->initialize($config);

        $dados['links_paginacao'] = $this->pagination->create_links();
        $dados['categorias'] =  $this->categorias;
        $dados['produtos'] = $this->ModelProdutos->listar_produto($pular,$prod_por_pagina);

        //cabeçalho
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Produto';

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        $this->load->model('Produtos_model', 'ModelProdutos');
        $dados['produtos'] = $this->ModelProdutos->buscar($pular, $prod_por_pagina);
        $this->load->view('backend/produto', $dados);
        $this->load->view('backend/template/footer');	
    }
    
    public function inserir(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-titulo', 'Título', 
        'required|min_length[5]');
        $this->form_validation->set_rules('txt-detalhes', 'Detalhes', 
        'required|min_length[15]');
        if ($this->form_validation->run() == FALSE){
            $this->index();
        }
        else{
            $titulo= $this->input->post('txt-titulo');
            $detalhes= $this->input->post('txt-detalhes');
            $preco= $this->input->post('txt-preco'); 
            $categoria= $this->input->post('select-categoria'); 
            if($this->ModelProdutos->adicionar($id, $titulo, $detalhes, $preco, $categoria)){
                $this->session->set_flashdata('categorias-ok',
                '<div class="alert alert-success">Item publicado com sucesso!</div>');
                redirect(base_url('index.php/admin/produto'));
            }
            else{
                echo "Houve um erro no sistema!";
            }
        }
    }

    public function excluir($id){

        if($this->ModelProdutos->excluir($id)){
            redirect(base_url('index.php/admin/produto'));
        }
        else{
            echo "Houve um erro no sistema!";
        }
    }

    public function alterar($id, $erro=null){
        $this->load->library('table');
        $dados['categorias'] =  $this->ModelCategorias->listar_categorias();
        $dados['produtos'] =  $this->ModelProdutos->listar_produtoss($id);
        if($erro){
            $dados['erros'] = $erro;
        }
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Produto';

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        $this->load->view('backend/alterar-produto');
        $this->load->view('backend/template/footer');
    }

    public function salvar_alteracoes($idCrip){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-titulo', 'Título', 
        'required|min_length[5]');
        $this->form_validation->set_rules('txt-detalhes', 'Detalhes', 
        'required|min_length[15]');
        if ($this->form_validation->run() == FALSE){
            $this->alterar($idCrip);
        }
        else{
            $titulo= $this->input->post('txt-titulo');
            $detalhes= $this->input->post('txt-detalhes');
            $preco= $this->input->post('txt-preco'); 
            $categoria= $this->input->post('select-categoria'); 
            $id = $this->input->post('txt-id');
            if($this->ModelProdutos->alterar($titulo, $detalhes, $preco, $categoria, $id)){
                redirect(base_url('index.php/admin/produto'));
            }
            else{
                echo "Houve um erro no sistema!";
            }
        }
    }

    public function nova_foto(){
        
        $id = $this->input->post('id');
        $config['upload_path'] = './assets/img/produto';
        
        $config['allowed_types'] = 'jpeg|jpg|png';
        $config['file_name'] = $id.".jpg";
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
            $erro = $this->upload->display_errors();
            return $this->alterar($id, $erro);
        }else{
            $config2['source_image'] = './assets/img/produto/'.$id.'.jpg';
            $config2['create_thumb'] = FALSE;
            $config2['width'] = 400;
            $config2['height'] = 400;
            $this->load->library('image_lib', $config2);
            
            if($this->image_lib->resize()){
                if($this->ModelProdutos->alterar_img($id)){
                    redirect(base_url('index.php/admin/produto/alterar/'.$id));
                    
                }
                else{
                    echo "Houve um erro no sistema!";
                }
            }else{
                $erro['erro'] = $this->image_lib->display_errors();
                echo $this->alterar($id, $erro);
            }
        }
    }

   


}