<?php

class Mensagens extends CI_Controller{
    public function __construct(){

        parent::__construct();
        //verificação login
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
    }

	public function index(){
        
        $this->load->model('Msg_model', 'ModelMsg');
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Mensagens';
        $dados['mensagens'] = $this->ModelMsg->gera_tabela();

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        
        $this->load->view('backend/table_lista', $dados);
        $this->load->view('backend/template/footer');
    }

    public function deletar($id){

        $this->load->model('Msg_model', 'ModelMsg');
        $this->ModelMsg->deletar($id);
        redirect('index.php/admin/mensagens');
    }
}