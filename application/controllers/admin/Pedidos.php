<?php

class Pedidos extends CI_Controller{
    public function __construct(){
		parent::__construct();
		
		//verificação login
		if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
    }

	public function index(){
        
        $this->load->model('Produtos_model', 'ModelProdutos');
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Pedidos';
        $dados['pedido'] = $this->ModelProdutos->gera_pedido();

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        
        $this->load->view('backend/table_pedidos', $dados);
        $this->load->view('backend/template/footer');
    }

    public function deletar($id){

        $this->load->model('Produtos_model', 'ModelProdutos');
        $this->ModelProdutos->deletar($id);
        redirect('index.php/admin/pedidos');
    }

    public function status($id=NULL){
		
		if($id == NULL) {
			redirect('/');
		}
		$this->load->model('Produtos_model', 'ModelProdutos');
		$query = $this->ModelProdutos->getProdutoByID($id);
		if($query != NULL) {
			if ($query->ativo == 1) {
				$dados['ativo'] = 0;
			} else {
				$dados['ativo'] = 1;
			}
			$this->ModelProdutos->statusPedido($dados, $query->id);
			redirect('index.php/admin/pedidos');
		} else {
			redirect('index.php/admin/pedidos');
		}

	}
}