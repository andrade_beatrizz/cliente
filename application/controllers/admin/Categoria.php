<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {
    public function __construct(){
        parent::__construct();
        //verificação login
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
        $this->load->model('categorias_model', 'ModelCategorias');
        $this->categorias = $this->ModelCategorias->listar_categorias();
    }

	public function index(){

        $this->load->library('table');
        $dados['categorias'] =  $this->categorias;

        //dados cabeçalho
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Categoria';

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        $this->load->view('backend/categoria');
        $this->load->view('backend/template/footer');
		
    }
    
    public function inserir(){

        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-categoria', 'Nome da categoria', 
        'required|min_length[5]|is_unique[categoria.titulo]');
        if ($this->form_validation->run() == FALSE){
            $this->index();
        }
        else{
            $titulo= $this->input->post('txt-categoria');
            if($this->ModelCategorias->adicionar($titulo)){
                $this->session->set_flashdata('categorias-ok',
                '<div class="alert alert-success">Item publicado com sucesso!</div>');
                redirect(base_url('index.php/admin/categoria'));
            }
            else{
                echo "Houve um erro no sistema!";
            }
        }
    }

    public function excluir($id){

        if($this->ModelCategorias->excluir($id)){
            redirect(base_url('index.php/admin/categoria'));
        }
        else{
            echo "Houve um erro no sistema!";
        }
    }

    public function alterar($id){
        $this->load->library('table');
        $dados['categorias'] =  $this->ModelCategorias->listar_categoria($id);

        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Categoria';

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        $this->load->view('backend/alterar-categoria');
        $this->load->view('backend/template/footer');
    }

    public function salvar_alteracoes(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-categoria', 'Nome da categoria', 
        'required|min_length[5]|is_unique[categoria.titulo]');
        if ($this->form_validation->run() == FALSE){
            $this->index();
        }
        else{
            $titulo= $this->input->post('txt-categoria');
            $id = $this->input->post('txt-id');
            if($this->ModelCategorias->alterar($titulo, $id)){
                redirect(base_url('index.php/admin/categoria'));
            }
            else{
                echo "Houve um erro no sistema!";
            }
        }
    }



}