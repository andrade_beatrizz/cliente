<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
    public function __construct(){
        parent::__construct();
    }

	public function index(){
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }

        $this->load->library('table');
        $this->load->model('usuarios_model', 'ModelUsuarios');
        $dados['usuarios'] = $this->ModelUsuarios->listar_autores();

        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Usuários';

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        $this->load->view('backend/usuarios');
        $this->load->view('backend/template/footer');
		
    }

    public function inserir(){
        
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
        $this->load->model('usuarios_model', 'ModelUsuarios');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-nome', 'Nome do Usuário', 
        'required|min_length[5]');
        $this->form_validation->set_rules('txt-email', 'E-mail do Usuário', 
        'required|valid_email');
        $this->form_validation->set_rules('txt-user', 'User', 
        'required|min_length[5]|is_unique[usuario.user]');
        $this->form_validation->set_rules('txt-senha', 'Senha do Usuário', 
        'required|min_length[5]');
        $this->form_validation->set_rules('txt-confsenha', 'Confirmar Senha', 
        'required|matches[txt-senha]');
        if ($this->form_validation->run() == FALSE){
            $this->index();
        }
        else{
            $nome = $this->input->post('txt-nome');
            $email = $this->input->post('txt-email');
            $user = $this->input->post('txt-user');
            $senha = $this->input->post('txt-senha');
            if($this->ModelUsuarios->adicionar($nome, $email, $user, $senha)){
                redirect(base_url('index.php/admin/usuarios'));
            }
            else{
                echo "Houve um erro no sistema!";
            }
        }
    }

    public function excluir($id){
        $this->load->model('usuarios_model', 'ModelUsuarios');
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }

        if($this->ModelUsuarios->excluir($id)){
            redirect(base_url('index.php/admin/usuarios'));
        }
        else{
            echo "Houve um erro no sistema!";
        }
    }

    public function alterar($id, $erro=null){
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
        $this->load->model('usuarios_model', 'ModelUsuarios');
        $dados['usuarios'] =  $this->ModelUsuarios->listar_usuario($id);
        if($erro){
            $dados['erros'] = $erro;
        }
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Usuários';

        $this->load->view('backend/template/header', $dados);
        $this->load->view('backend/template/template');
        $this->load->view('backend/alterar-usuario');
        $this->load->view('backend/template/footer');
    }

    public function salvar_alteracoes($idCrip){
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
        $this->load->model('usuarios_model', 'ModelUsuarios');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt-nome', 'Nome do Usuário', 
        'required|min_length[5]');
        $this->form_validation->set_rules('txt-email', 'E-mail do Usuário', 
        'required|valid_email');
        $this->form_validation->set_rules('txt-user', 'User', 
        'required|min_length[5]|is_unique[usuario.user]');
        $senha = $this->input->post('txt-senha');
        if($senha != ""){
            $this->form_validation->set_rules('txt-senha', 'Senha do Usuário', 
            'required|min_length[5]');
            $this->form_validation->set_rules('txt-confsenha', 'Confirmar Senha', 
            'required|matches[txt-senha]'); 
        }
        
        if ($this->form_validation->run() == FALSE){
            $this->alterar($idCrip);
        }
        else{
            $nome = $this->input->post('txt-nome');
            $email = $this->input->post('txt-email');
            $user = $this->input->post('txt-user');
            $senha = $this->input->post('txt-senha');
            $id = $this->input->post('txt-id');
            if($this->ModelUsuarios->alterar($id, $nome, $email, $user, $senha)){
                redirect(base_url('index.php/admin/usuarios'));
            }
            else{
                echo "Houve um erro no sistema!";
            }
        }
    }

    public function nova_foto(){
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
        $this->load->model('usuarios_model', 'ModelUsuarios');
        $id = $this->input->post('id');
        $config['upload_path'] = './assets/img/usuarios';
        $config['allowed_types'] = 'jpg|png';
        $config['file_name'] = $id.".jpg";
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload()){
            $erro = $this->upload->display_errors();
            return $this->alterar($id, $erro);
        }else{
            $config2['source_image'] = './assets/img/usuarios/'.$id.'.jpg';
            $config2['create_thumb'] = FALSE;
            $config2['width'] = 200;
            $config2['height'] = 200;
            $this->load->library('image_lib', $config2);
            if($this->image_lib->resize()){
                if($this->ModelUsuarios->alterar_img($id)){
                redirect(base_url('index.php/admin/usuarios/alterar/'.$id));
                    
                }
                else{
                    echo "Houve um erro no sistema!";
                }
            }else{
                $erro['erro'] = $this->image_lib->display_errors();
                echo $this->alterar($id, $erro);
            }
        }
    }
    
    public function page_login(){
        if($this->session->userdata('logado')){
            redirect(base_url('index.php/usuario'));
        }
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Área de acesso';

        $this->load->view('common/header', $dados);
        
        $this->load->view('backend/login');
        
        $this->load->view('common/footer');
    }

    public function login(){
        $this->load->library('form_validation');

        // Login
        $this->form_validation->set_rules('txt-user', 'Usuário', 
        'required|min_length[5]');
        $this->form_validation->set_rules('txt-senha', 'Senha', 
        'required|min_length[5]');
        if ($this->form_validation->run() == FALSE){
            $this->page_login();
        }else{
            $usuario = $this->input->post('txt-user');
            $senha = $this->input->post('txt-senha');


            $this->db->where('user', $usuario);
            $this->db->where('senha', md5($senha));                          
            $userlogado = $this->db->get('usuario')->row_object();

            if(isset($userlogado->id)){
                $dadosSessao['userlogado'] = $userlogado;
                $dadosSessao['logado'] = TRUE;
                $this->session->set_userdata($dadosSessao);
                
                if($userlogado->tipo > 1) {
                    redirect(base_url('index.php/admin'));
                } else {
                    redirect('index.php/usuario');
                   
                }
            }else{
                $dadosSessao['userlogado'] = NULL;
                $dadosSessao['logado'] = FALSE;
                $this->session->set_userdata($dadosSessao);
                redirect(base_url('index.php/admin/login')); 
            }
            
        }
        // fim login
    }

    public function logout(){
        $dadosSessao['userlogado'] = NULL;
        $dadosSessao['logado'] = FALSE;
        $this->session->set_userdata($dadosSessao);
        redirect(base_url('index.php/admin/login'));
    }

    

}