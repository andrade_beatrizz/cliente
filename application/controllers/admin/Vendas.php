<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendas extends CI_Controller {
    
    public function __construct(){
        parent::__construct();

        //verificação login
        if(!$this->session->userdata('logado')){
            redirect(base_url('index.php/admin/login'));
        }
        if($this->session->userdata('userlogado')->tipo < 2){
            redirect(base_url('index.php/usuario'));
        }
        $this->load->model('categorias_model', 'ModelCategorias');
        $this->categorias = $this->ModelCategorias->listar_categorias();  
    }
    
	public function index(){
        $this->load->helper('funcoes');
        $dados['categorias'] =  $this->categorias;
        $this->load->model('produtos_model', 'ModelProdutos');
        $dados['produtos'] = $this->ModelProdutos->listar_produtos();

        $dados['titulo'] = 'Painel de controle';
        $dados['subtitulo'] = 'Vendas';
        
        $this->load->model('Produtos_model', 'ModelProdutos');
        $this->load->view('backend/template/header', $dados);
        
        $this->load->view('backend/template/template');
        $dados['pedido'] = $this->ModelProdutos->vendas();
        $this->load->view('backend/vendas', $dados);
    
        $this->load->view('backend/template/footer');
        
		
	}

}