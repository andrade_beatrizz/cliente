<?php 

class Cadastro extends Ci_Controller{

    public function __construct(){
        parent::__construct();
    }

    public function user(){
        //dados cabeçalho
        $dados['titulo'] = 'Painel de Controle';
        $dados['subtitulo'] = 'Área de acesso';

        $this->load->view('common/header', $dados);
        $this->load->view('frontend/cadastro');
        $this->load->view('common/footer');
    }

    public function novo(){
       
        //cadastro
        $this->load->model('usuarios_model', 'ModelUsuarios');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nome', 'Nome do Usuário', 
        'required|min_length[5]');
        $this->form_validation->set_rules('email', 'E-mail do Usuário', 
        'required|valid_email');
        $this->form_validation->set_rules('telefone', 'Telefone do Usuário', 
        'required|min_length[10]|max_length[11]');
        $this->form_validation->set_rules('user', 'User', 
        'required|min_length[5]|is_unique[usuario.user]');
        $this->form_validation->set_rules('senha', 'Senha do Usuário', 
        'required|min_length[5]');
        $this->form_validation->set_rules('confsenha', 'Confirmar Senha', 
        'required|matches[senha]');
        if ($this->form_validation->run() == FALSE){
            $this->user();
        }
        else{
            $nome = $this->input->post('nome');
            $email = $this->input->post('email');
            $telefone = $this->input->post('telefone');
            $user = $this->input->post('user');
            $passwd = $this->input->post('senha');
            if($this->ModelUsuarios->adicionar($nome, $email, $telefone, $user, $passwd)){
                $this->session->set_flashdata('user-ok',
                '<div class="alert alert-success">Usuário cadastrado com sucesso!</div>');
                redirect(base_url('index.php/usuario'));
            }
            else{
                echo "Houve um erro no sistema!";
            }
        }
    }
}