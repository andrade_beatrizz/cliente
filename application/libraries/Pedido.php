<?php
include_once APPPATH. 'libraries/util/CI_Object.php';

class Pedido extends CI_Object{
    
    public function lista(){
        $rs = $this->db->get('pedido');
        $result = $rs->result_array();
        return $result;
    }

    public function gera_pedido($data){
        $this->db->insert('pedido', $data);
        return $this->db->insert_id();
    }

    public function delete($id){
        $cond = array('id' => $id);
        $this->db->delete('pedido', $cond);

    }
  
}