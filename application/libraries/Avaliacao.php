<?php
include_once APPPATH. 'libraries/util/CI_Object.php';

class Avaliacao extends CI_Object{
    
    public function lista_avaliacao($id){
        $this->db->where('id ='.$id);
        $rs = $this->db->get('avaliacao_produto');
        $result = $rs->result_array();
        return $result;
    }

    public function gera_avaliacao($data){
        $this->db->insert('avaliacao_produto', $data);
        return $this->db->insert_id();
    }

    public function delete($id){
        $cond = array('id' => $id);
        $this->db->delete('avaliacao_produto', $cond);

    }
}