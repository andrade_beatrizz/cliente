<?php
include_once APPPATH. 'libraries/util/CI_Object.php';

class Msg extends CI_Object{
    
    public function lista(){
        $rs = $this->db->get('mensagens');
        $result = $rs->result_array();
        return $result;
    }

    public function gera_mensagem($data){
        $this->db->insert('mensagens', $data);
        return $this->db->insert_id();
    }

    public function delete($id){
        $cond = array('id' => $id);
        $this->db->delete('mensagens', $cond);

    }
}