<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'default';
$query_builder = TRUE;

if(ENVIRONMENT != 'development'){
	$username = 'gu1800337';
	$password = 'I+Kh8DZ1Sw1I8ZKa6+2J2JVsr9ctkBtgwp8Qx6A3kQY=';
	$database = 'gu1800337';
}else{
	$username = 'root';
	$password = '';
	$database = 'teste';
}

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => 'localhost',
	'username' => $username,
	'password' => $password,
	'database' => $database,
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
