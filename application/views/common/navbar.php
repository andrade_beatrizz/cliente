<nav class="mb-1 navbar navbar-expand-lg navbar-dark fixed-top amber">
  <div class="img-responsive">
    <a href="<?= base_url('index.php');?>">
    <img src="<?=base_url('assets/mdb/img/icons/logotipo.png') ?>" 
    width="150" height="150" class="logotipo"/>
    </a>
  </div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
  <a class="nav-link font-weight-bolder h5 text-white" href="<?= base_url('');?>">Início</a>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">  
        <a class="nav-link font-weight-bolder dropdown-toggle h5" id="navbarDropdownMenuLink-333" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Categorias
        </a>
            
  <div class="dropdown-menu dropdown-default amber" aria-labelledby="navbarDropdownMenuLink-333">
    <?php 
      foreach($categorias as $categoria){
    ?>
      <div id="drop">
        <p>
          <a class="text-white" href="<?= base_url('index.php/categoria/'.$categoria->id.'/'.limpar($categoria->titulo)) ?>"><?php echo $categoria->titulo ?></a>
        </p>
      </div>
    <?php
      }
    ?>
  </div>
              <li class="nav-item">
                <a class="nav-link font-weight-bolder h5" href="<?= base_url('index.php/sobrenos');?>">Mini Blog</a>
              </li>

              <li class="nav-item">
                <a class="nav-link font-weight-bolder h5" href="<?= base_url('index.php/contato');?>">Contato</a>
              </li>
    </ul>

    
      <ul class="navbar-nav ml-auto nav-flex-icons">
        <?php if($this->session->userdata('logado')){?>  
          <?php if($this->session->userdata('userlogado')->tipo > 1){ ?>
            <li class="active"><a href="<?= base_url('index.php/admin')?>" title="Administrar"><i class="text-white fas fa-user-cog pr-3"></i></a></li>
          <?php } ?> 
          <?php if($this->session->userdata('userlogado')->tipo < 2){ ?>
           <li class="active"><a href="<?= base_url('index.php/login')?>" title="Meus pedidos"> <i class="text-white fas fa-shopping-cart pr-3"></i></a></li>
          <?php } ?>
            <li class="active"><a href="<?= base_url('index.php/usuario/logout')?>" title="Logout"><i class="text-white fas fa-sign-out-alt"></i></a></li>
          <?php }else
            if(!$this->session->userdata('logado')) {?>
            <li class="active"><a href="<?= base_url('index.php/admin/login')?>" title="Login"><i class="text-white far fa-user pr-3"></i></a></li>
          <?php }?>
      </ul>
  </div>
</nav>
