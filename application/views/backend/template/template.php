<div id="wrapper">
        <nav>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Navegação</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?= base_url('index.php/admin')?>">Painel Administrativo</a>
            </div>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?= base_url('index.php/admin/categoria')?>"><i class="fa fa-sitemap fa-fw"></i> Categorias</a>
                        </li>
                        <li>
                            <a href="<?= base_url('index.php/admin/produto')?>"><i class="fa fa-edit fa-fw"></i> Produtos</a>
                        </li>
                        <li>
                            <a href="<?= base_url('index.php/admin/usuarios')?>"><i class="fa fa-wrench fa-fw"></i> Usuários</a>
                        </li>
                        <li>
                            <a href="<?= base_url('index.php/admin/mensagens')?>"><i class="fa fa-envelope fa-fw"></i> Mensagens</a>
                        </li>
                        <li>
                            <a href="<?= base_url('index.php/admin/pedidos')?>"><i class="fas fa-shopping-cart"></i> Pedidos</a>
                        </li>
                        <li>
                            <a href="<?= base_url('index.php/admin/vendas')?>"><i class="fas fa-check-square"></i> Vendas</a>
                        </li>
                        <li>
                            <a href="<?= base_url('index.php/Ans')?>"><i class="fas fa-angle-double-left fa-fw"></i> Voltar </a>
                        </li>
                        <li>
                            <a href="<?= base_url('index.php/admin/usuarios/logout')?>"><i class="fa fa-times fa-fw"></i> Sair do Sistema</a>
                        </li>
                
                    </ul>
                </div>
            </div>
        </nav>
