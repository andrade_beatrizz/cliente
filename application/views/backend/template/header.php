<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="shortcut icon" href="<?= base_url('assets/mdb/img/icons/favicon.png') ?>">
    <meta name="author" content="">

    <title>
        <?php echo $titulo.' - '.$subtitulo?>
    </title>

    <link href="<?= base_url('assets/backend/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/backend/css/sb-admin-2.min.css" rel="stylesheet')?>">
    <link href="<?= base_url('assets/backend/css/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
   
</head>
<body>