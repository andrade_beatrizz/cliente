<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <?php echo 'Administrar '.$subtitulo ?>
                    </h1>
                </div>
            </div>

            <!-- ADICIONAR PRODUTO -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo 'Adicionar novo '.$subtitulo?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    echo $this->session->flashdata('categorias-ok');
                                    echo validation_errors('<div class="alert alert-danger">','</div>');
                                    echo form_open('index.php/admin/produto/inserir');
                                   ?>

                                   <div class="form-group">
                                        <label id="select-categoria">Categoria</label>
                                        <select id="select-categoria" name="select-categoria" class="form-control">
                                        <?php
                                            foreach($categorias as $categoria){  
                                        ?>
                                        <option value="<?php echo $categoria->id ?>">
                                                <?php
                                                echo $categoria->titulo 
                                                ?>
                                        </option>
                                            <?php 
                                            } 
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-titulo">Título do produto</label>
                                            <input type="text" id="txt-titulo" name="txt-titulo" 
                                            class="form-control" placeholder="Digite o titulo do produto" 
                                            value="<?php echo set_value('txt-titulo')?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-detalhes">Detalhes</label>
                                            <input type="text" id="txt-detalhes" name="txt-detalhes" 
                                            class="form-control"
                                            value="<?php echo set_value('txt-detalhes')?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-preco">Preço</label>
                                            <input type="text" id="txt-preco" name="txt-preco" 
                                            class="form-control" placeholder="Digite o preço"
                                            value="<?php echo set_value('txt-preco')?>">
                                    </div>
                                    <button type="submit" class="btn btn-default"> Cadastrar </button>
                                    
                                   <?php
                                    echo form_close();
                                   ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ALTERAR PRODUTO -->
                <div class="col-lg-12">
                    <div class=" panel-default">
                        <h2 class="for">
                            <?php echo 'Alterar '.$subtitulo.' existente'?>
                        </h2>
                        

                        <form class="form-inline" action="<?= base_url('index.php/admin/produto') ?>" method="POST">
                            <div class="md-form my-0 mr-5">
                            <input class="form-control mr-sm-2" name="busca" id="busca" type="text" placeholder="Busca" aria-label="Search">
                            <button type="submit" class="btn btn-amber"><i class="far fa-paper-plane"></i></button>
                            </div>
                        </form>
                        <hr color="black">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <style>
                                    img{
                                        width:110px;
                                    }
                                    </style>
                                   <?php
                                    $this->table->set_heading("Foto", "Titulo","Detalhes", "Preço", "Alterar", "Excluir");
                                    foreach($produtos as $produto){
                                        $titulo = $produto->titulo; 
                                         
                                        
                                            $this->load->helper("html");
                                            if($produto->img == 1){
                                            $fotoprod = img("assets/img/produto/".md5($produto->id).".jpg"); 
                                            }else{
                                            $fotoprod = img("assets/img/semfoto.jpg"); 
                                            }
                                        
                                        $detalhes = $produto->detalhes;
                                        $preco = $produto->preco;
                                        $alterar= anchor(base_url('index.php/admin/produto/alterar/'.md5($produto->id)),
                                        '<i class="fa fa-refresh fa-fw"></i> Alterar');
                                        $excluir= '<button type="button" class="btn btn-link" data-toggle="modal" data-target=".excluir-modal-'.$produto->id.'"><i class="fa fa-remove fa-fw"></i> Excluir</button>';

                                        echo $modal= ' <div class="modal fade excluir-modal-'.$produto->id.'" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel2">Exclusão de Produto</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h4>Deseja Excluir o produto '.$produto->titulo.'?</h4>
                                                <p>Após Excluido o produto <b>'.$produto->titulo.'</b> não ficara mais disponível no Sistema.</p>
                                                <p>Todos os itens relacionados ao produto <b>'.$produto->titulo.'</b> serão afetados e não aparecerão no site até que sejam editados.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                <a type="button" class="btn btn-warning" href="'.base_url("index.php/admin/produto/excluir/".md5($produto->id)).'">Excluir</a>
                                            </div>

                                        </div>
                                        </div>
                                         </div>';

                                        $this->table->add_row($fotoprod, $titulo, $detalhes, $preco, $alterar, $excluir);
                                    } 

                                    $this->table->set_template(array(
                                        'table_open' => '<table class="table">'
                                    ));
                                    echo $this->table->generate();
                                    echo "<div class='paginacao'>".$links_paginacao."</di>";
                                   ?>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

 


