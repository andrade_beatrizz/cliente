<div id="page-wrapper">
    <div class="col-lg-12">
        <h1 class="page-header">
            Maiores vendas do mês
        </h1>
    </div>
        <table class="table">
            <thead class="h4">
                <th>Nome do Produto</th>
                <th>Quantidade</th>
                <th>Mês de referência</th>
                <th>Ano de referência</th>
            </thead>
            <tbody>
                <tr>
                <?php
                    foreach ($pedido as $pedido){ ?>
                        <td><?php echo $pedido->titulo_prod ?></td>
                        <td><?php echo $pedido->quantidade_prod ?></td>
                        <td><?php echo $pedido->mes ?></td>
                        <td><?php echo $pedido->ano ?></td>
                </tr>
            </tbody>
                <?php 
                    } 
                ?>
        </table>
</div>