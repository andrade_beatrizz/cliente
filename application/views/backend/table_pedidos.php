<div id="page-wrapper">
    <div class="col-lg-12">
        <h1 class="page-header">
            Pedidos
        </h1>
    </div>
        <table class="table">
            <thead>
                <th>Nome Usuário</th>
                <th>E-mail</th>
                <th>Telefone</th>
                <th>Produto</th>
                <th>Detalhes</th>
                <th>Quantidade</th>
                <th>Preço Un.</th>
                <th>Preço total</th>
                <th>Status</th>
                <th>Ações</th>
                
            </thead>
            <tbody>
                <tr>
                <?php
                    foreach ($pedido as $pedido){ ?>
                        <td><?php echo $pedido->userlogado ?></td>
                        <td><?php echo $pedido->email_user ?></td>
                        <td><?php echo $pedido->telefone_user ?></td>
                        <td><?php echo $pedido->titulo_prod ?></td>
                        <td><?php echo $pedido->detalhes_prod ?></td>
                        <td><?php echo $pedido->quantidade_prod ?></td>
                        <td><?php echo $pedido->preco_prod ?></td>
                        <td><?php echo $pedido->total?></td>
                <?php
                    //status do produto
                    if ($pedido->ativo == 1) { ?>
                        <td><a href="<?= base_url('index.php/admin/pedidos/status/'.$pedido->id) ?>"><i class="fas fa-check"></i></td>;
                <?php   
                    }else{ ?>
                        <td><a href="<?= base_url('index.php/admin/pedidos/status/'.$pedido->id)?>"><i class="fas fa-times"></i></td>
                <?php
                    } 
                ?>
                        <td><a href=<?= base_url('index.php/admin/pedidos/deletar/'.$pedido->id) ?>><i class="fas fa-trash"></i></a></td>
                </tr>
            </tbody>
                <?php 
                    } 
                ?>
        </table>
</div>