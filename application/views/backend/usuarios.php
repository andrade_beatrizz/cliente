<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <?php echo 'Administrar '.$subtitulo ?>
                    </h1>
                </div>
            </div>

            <div class="row">
                <!-- ADICIONAR USUÁRIO -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo 'Adicionar novo '.$subtitulo?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                   <?php
                                    echo validation_errors('<div class="alert alert-danger">','</div>');
                                    echo form_open('index.php/admin/usuarios/inserir');
                                   ?>
                                    <div class="form-group">
                                            <label id="txt-nome">Nome do usuário</label>
                                            <input type="text" id="txt-nome" name="txt-nome" 
                                            class="form-control" placeholder="Digite o nome do usuário" 
                                            value="<?php echo set_value('txt-nome')?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-email">E-mail</label>
                                            <input type="text" id="txt-email" name="txt-email" 
                                            class="form-control" placeholder="Digite o e-mail do usuário"
                                            value="<?php echo set_value('txt-email')?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-user">User</label>
                                            <input type="text" id="txt-user" name="txt-user" 
                                            class="form-control" placeholder="Digite o nickname do usuário"
                                            value="<?php echo set_value('txt-user')?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-senha">Senha</label>
                                            <input type="password" id="txt-senha" name="txt-senha" 
                                            class="form-control" placeholder="Digite a senha do usuário">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-confsenha">Confirmar Senha</label>
                                            <input type="password" id="txt-confsenha" name="txt-confsenha" 
                                            class="form-control" placeholder="Confirme a senha">
                                    </div>
                                    <button type="submit" class="btn btn-default">Adicionar</button>
                                   <?php
                                    echo form_close();
                                   ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ALTERAR USUÁRIO -->
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo 'Alterar '.$subtitulo.' existente'?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                <style>
                                img{
                                    width:120px;
                                }
                                </style>
                                   <?php
                                    $this->table->set_heading("Foto", "Nome do usuário", "Alterar", "Excluir");
                                    foreach($usuarios as $usuario){
                                        $nomeuser = $usuario->nome; 
                                         	
                                        $this->load->helper("html");
                                        if($usuario->img == 1){
                                        $fotouser= img("assets/img/usuarios/".md5($usuario->id).".jpg"); 
                                        }else{
                                        $fotouser= img("assets/img/semfoto.jpg");
                                        }
                                        
                                        $alterar= anchor(base_url('index.php/admin/usuarios/alterar/'.md5($usuario->id)),
                                        '<i class="fa fa-refresh fa-fw"></i> Alterar');
                                        $excluir= '<button type="button" class="btn btn-link" data-toggle="modal" data-target=".excluir-modal-'.$usuario->id.'"><i class="fa fa-remove fa-fw"></i> Excluir</button>';

                                        echo $modal= ' <div class="modal fade excluir-modal-'.$usuario->id.'" tabindex="-1" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog modal-sm">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel2">Exclusão de usuário</h4>
                                            </div>
                                            <div class="modal-body">
                                                <h4>Deseja Excluir o usuário '.$usuario->nome.'?</h4>
                                                <p>Após Excluido o usuário <b>'.$usuario->nome.'</b> não ficara mais disponível no Sistema.</p>
                                                <p>Todos os itens relacionados ao usuário <b>'.$usuario->nome.'</b> serão afetados e não aparecerão no site até que sejam editados.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>

                                                <a type="button" class="btn btn-warning" href="'.base_url("index.php/admin/usuarios/excluir/".md5($usuario->id)).'">Excluir</a>

                                            </div>

                                        </div>
                                        </div>
                                         </div>';

                                        $this->table->add_row($fotouser, $nomeuser, $alterar, $excluir);
                                    } 

                                    $this->table->set_template(array(
                                        'table_open' => '<table class="table table-striped">'
                                    ));
                                    echo $this->table->generate();
                                   ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>


