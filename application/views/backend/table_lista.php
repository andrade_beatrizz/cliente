<div id="page-wrapper">
    <div class="col-lg-12">
        <h1 class="page-header">
            Mensagens
        </h1>
    </div>
        <table class="table">
            <thead>
                <th>Nome</th>
                <th>Telefone</th>
                <th>E-mail</th>
                <th>Mensagem</th>
                <th>Ações</th>
                
            </thead>
            <tbody>
                <tr>
                <?php
                    foreach ($mensagens as $mensagem){ ?>
                        <td><?php echo $mensagem->nome ?></td>
                        <td><?php echo $mensagem->telefone ?></td>
                        <td><?php echo $mensagem->email ?></td>
                        <td><?php echo $mensagem->mensagem ?></td>
                        <td><a href=<?= base_url('index.php/admin/mensagens/deletar/'.$mensagem->id) ?>><i class="fas fa-trash"></i></a></td>
                </tr>
            </tbody>
                <?php 
                    } 
                ?>
        </table>
</div>