<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <?php echo 'Administrar '.$subtitulo ?>
                    </h1>
                </div>
            </div>

            <div class="row">
               
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo 'Adicionar novo '.$subtitulo?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                   <?php
                                    echo validation_errors('<div class="alert alert-danger">','</div>');
                                    foreach($usuarios as $usuario){
                                    echo form_open('index.php/admin/usuarios/salvar_alteracoes/'.md5($usuario->id));
                                    
                                   ?>
                                    <div class="form-group">
                                            <label id="txt-nome">Nome do usuário</label>
                                            <input type="text" id="txt-nome" name="txt-nome" 
                                            class="form-control" placeholder="Digite o nome do usuário" 
                                            value="<?php echo $usuario->nome ?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-email">E-mail</label>
                                            <input type="text" id="txt-email" name="txt-email" 
                                            class="form-control" placeholder="Digite o e-mail do usuário"
                                            value="<?php echo $usuario->email ?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-user">User</label>
                                            <input type="text" id="txt-user" name="txt-user" 
                                            class="form-control" placeholder="Digite o user do usuário"
                                            value="<?php echo $usuario->user ?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-senha">Senha</label>
                                            <input type="password" id="txt-senha" name="txt-senha" 
                                            class="form-control" placeholder="Digite a senha do usuário">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-confsenha">Confirmar Senha</label>
                                            <input type="password" id="txt-confsenha" name="txt-confsenha" 
                                            class="form-control" placeholder="Confirme a senha">
                                    </div>
                                    <input type="hidden" name="txt-id" id="txt-id" value="<?php echo $usuario->id ?>">
                                    <button type="submit" class="btn btn-default">Atualizar</button>
                                   <?php
                                    
                                    echo form_close();
                                   ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                <?php if(isset($erros)){ ?>
                    <div class="alert alert-danger">
                        <?=$erros?>
                    </div>
                 <?php } ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        
                            <?php echo 'Imagem de destaque do '.$subtitulo ?>
                        </div>
                        <div class="panel-body">
                            <div class="row" style="padding-bottom:10">
                                <div class="col-lg-3 col-lg-offset-3">
                                <?php 	
                                $this->load->helper("html");
                                if($usuario->img == 1){
                                echo img("assets/img/usuarios/".md5($usuario->id).".jpg"); 
                                }else{
                                echo img("assets/img/semfoto.jpg"); 
                                }
                                
                                ?>
                                </div>                            
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                   <?php
                                   $divopen= '<div class="form-group">';
                                   $divclose= '</div>';
                                    
                                    echo form_open_multipart('index.php/admin/usuarios/nova_foto');
                                    echo form_hidden('id', md5($usuario->id));
                                    echo $divopen;
                                    $imagem= array('name' => 'userfile', 'id' => 'userfile', 'class' => 'form-control', 'required' => '', 'accept' => 'image/.jpg');
                                    echo form_upload($imagem);
                                    echo $divclose;
                                    echo $divopen;
                                    $botao= array('name' => 'btn_adicionar', 'id' => 'btn_adicionar', 'class' => 'btn btn-default',
                                    'value' => 'Adicionar nova imagem');
                                    echo form_submit($botao);
                                    echo $divclose;
                                    echo form_close();
                                    }
                                   ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


