<style type="text/css">
    body{
        background-image: url(<?=base_url('assets/mdb/img/about/log.jpg') ?>);
        background-repeat: no-repeat;
    }
</style>

<a href="<?= base_url('index.php');?>">
    <center><img class="imglogin" src="<?= base_url('assets/img/cadastro.png')?>" width="200" height="200"/></center>
</a>
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-5 col-md-offset-5">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h1 class="for text-center mb-4 text-black">Login</h1>
                        </div>
                        
                        <div class="panel-body">
                            <?php
                                echo validation_errors('<div class="alert alert-danger">','</div>');
                                echo form_open('index.php/admin/usuarios/login');
                            ?>
                                
                                    <div class="form-group">
                                    <label id="txt-nome" class="form text-white">Nome de usuário</label>
                                        <input class="form-control" placeholder="Usuário" name="txt-user" type="text" autofocus>
                                    </div>
                                    <label id="txt-nome" class="form text-white">Senha</label>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Senha" name="txt-senha" type="password" value="">
                                    </div>
                                    <button class="btn btn-lg btn-warning btn-block form">Entrar</button>
                                    <hr>
                                    
                            <?php   
                                echo form_close();
                            ?>
                        </div>
                        <p class="form mt-4 text-white"> Ainda não possui uma conta? <a id="log" href="<?= base_url('index.php/admin/cadastro/user') ?>">Cadastre-se</a></p>
                    </div>
                </div>
            </div>
        </div>

  