<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php echo 'Administrar '.$subtitulo ?>
                </h1>
            </div>
        </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo 'Administrar '.$subtitulo?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <?php
                                    echo validation_errors('<div class="alert alert-danger">','</div>');
                                    foreach($produtos as $produto){
                                    echo form_open('index.php/admin/produto/salvar_alteracoes/'.$produto->id);
                                    
                                   ?>

                                   <div class="form-group">
                                        <label id="select-categoria">Categoria</label>
                                        <select id="select-categoria" name="select-categoria" class="form-control">
                                        <?php
                                            foreach($categorias as $categoria){  ?>
                                                <option value="<?php echo $categoria->id ?>" <?php if($categoria->id 
                                                == $produto->categoria){echo "selected";}?>>
                                                <?php
                                                    echo $categoria->titulo 
                                                ?>
                                                </option>
                                                <?php } 
                                                ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-titulo">Título do produto</label>
                                            <input type="text" id="txt-titulo" name="txt-titulo" 
                                            class="form-control" placeholder="Digite o titulo do produto" 
                                            value="<?php echo $produto->titulo ?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-detalhes">Detalhes</label>
                                            <input type="text" id="txt-detalhes" name="txt-detalhes" 
                                            class="form-control"
                                            value="<?php echo $produto->detalhes ?>">
                                    </div>
                                    <div class="form-group">
                                            <label id="txt-preco">Preço</label>
                                            <input type="text" id="txt-preco" name="txt-preco" 
                                            class="form-control" placeholder="Digite o preço"
                                            value="<?php echo $produto->preco ?>">
                                    </div>
                                    <input type="hidden" name="txt-id" value="<?php echo $produto->id ?>">
                                    <button type="submit" class="btn btn-default">Salvar alterações</button>
                                    
                                   <?php
                                    echo form_close();
                                   ?>
                                </div>
                                
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <div class="col-lg-6">
                <?php if(isset($erros)){ ?>
                    <div class="alert alert-danger">
                        <?=$erros?>
                    </div>
                 <?php } ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        
                            <?php echo 'Imagem de destaque do '.$subtitulo ?>
                        </div>
                        <style type="text/css">
                            img{width:200px;};
                        </style>
                        <div class="panel-body">
                            <div class="row" style="padding-bottom:10">
                                <div class="col-lg-8 col-lg-offset-3">
                                <?php 	
                                $this->load->helper("html");
                                if($produto->img == 1){
                                echo img("assets/img/produto/".md5($produto->id).".jpg"); 
                                }else{
                                echo img("assets/img/semfoto.jpg"); 
                                }
                                
                                ?>
                                </div>                            
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                   <?php
                                   $divopen= '<div class="form-group">';
                                   $divclose= '</div>';
                                    
                                    echo form_open_multipart('index.php/admin/produto/nova_foto');
                                    echo form_hidden('id', md5($produto->id));
                                    echo $divopen;
                                    $imagem= array('name' => 'userfile', 'id' => 'userfile', 'class' => 'form-control', 'required' => '', 'accept' => 'image/.jpg');
                                    echo form_upload($imagem);
                                    echo $divclose;
                                    echo $divopen;
                                    $botao= array('name' => 'btn_adicionar', 'id' => 'btn_adicionar', 'class' => 'btn btn-default',
                                    'value' => 'Adicionar nova imagem');
                                    echo form_submit($botao);
                                    echo $divclose;
                                    echo form_close();
                                    }
                                   ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>


 


