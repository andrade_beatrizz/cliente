<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php echo 'Administrar '.$subtitulo ?>
                </h1>
            </div>
        </div>

            <div class="row">

                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo 'Alterar '.$subtitulo?>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                   <?php
                                    echo validation_errors('<div class="alert alert-danger">','</div>');
                                    echo form_open('index.php/admin/categoria/salvar_alteracoes');
                                    foreach($categorias as $categoria){
                                   ?>
                                    <div class="form-group">
                                            <label id="txt-categoria">Nome da Categoria</label>
                                            <input type="text" id="txt-categoria" name="txt-categoria"
                                            class="form-control" placeholder="Digite o nome da categoria"
                                            value="<?php echo $categoria->titulo ?>">
                                    </div>
                                    <input type="hidden" name="txt-id" id="txt-id" value="<?php echo $categoria->id ?>">
                                    <button type="submit" class="btn btn-default">Atualizar</button>
                                   <?php
                                    }
                                    echo form_close();
                                   ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
