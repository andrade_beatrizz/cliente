<style type="text/css">
    body{
        background-image: url(<?=base_url('assets/mdb/img/about/log.jpg') ?>);
        background-repeat: no-repeat;

    }
</style>
<center><img class="image" src="<?= base_url('assets/img/cadastro.png')?>" height="200" width="200"/></center>
    <div class="container mt-5 ">
        <div class="row">
            <div class="col-md-9 col-lg-5 ml-5">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="for text-center mb-4">Cadastre-se</h3>
                    </div>
                    <div class="panel-body">
                    <?php
                        echo $this->session->flashdata('user-ok');
                        echo validation_errors('<div class="alert alert-danger">','</div>');
                        echo form_open('index.php/admin/cadastro/novo');
                    ?>
                                
                        <div class="form-group">
                            <label id="nome" class="form text-white">Nome do usuário</label>
                            <input type="text" id="nome" name="nome" 
                            class="form-control" placeholder="Digite o nome do usuário" 
                            value="<?php echo set_value('nome')?>">
                        </div>
                        <div class="form-group">
                            <label id="email" class="form text-white">E-mail</label>
                            <input type="text" id="email" name="email" 
                            class="form-control" placeholder="Digite o e-mail do usuário"
                            value="<?php echo set_value('email')?>">
                        </div>
                        <div class="form-group">
                            <label id="telefone" class="form text-white">Telefone</label>
                            <input type="number" id="telefone" name="telefone" 
                            class="form-control" placeholder="(XX) XXXXXXXXX"
                            value="<?php echo set_value('telefone')?>">
                        </div>
                        <div class="form-group">
                            <label id="user" class="form text-white">User</label>
                            <input type="text" id="user" name="user" 
                            class="form-control" placeholder="Digite o nickname do usuário"
                            value="<?php echo set_value('user')?>">
                        </div>
                        <div class="form-group">
                            <label id="senha" class="form text-white">Senha</label>
                            <input type="password" id="senha" name="senha" 
                            class="form-control" placeholder="Digite a senha do usuário">
                        </div>
                        <div class="form-group">
                            <label id="confsenha" class="form text-white">Confirmar Senha</label>
                            <input type="password" id="confsenha" name="confsenha" 
                            class="form-control" placeholder="Confirme a senha">
                        </div>
                            <button type="submit" class="btn btn-lg btn-warning btn-block form mb-4">Cadastrar usuário</button>
                    <?php
                        echo form_close();
                    ?>
                    </div>
                </div>
            </div> 
                
        </div>
    </div>
