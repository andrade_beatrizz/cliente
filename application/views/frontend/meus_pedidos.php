<div class="container mt-5"> <br><br><br><br>
        <div id="page-wrapper">
            <div class="col-lg-12">
            <?php  echo $this->session->flashdata('categorias-ok'); ?>
                <h2 class="for">
                    Meus Pedidos
                </h2>
            </div>
        
            <table class="table">
                <thead>
                    
                    <th>Produto</th>
                    <th>Detalhes</th>
                    <th>Quantidade</th>
                    <th>Total</th>
                    <th>Status</th>
                    
                </thead>
                <tbody>
                    <tr>
                        <?php
                            foreach ($pedido as $pedido){ 
                        ?>
                     
                            <td><?php echo $pedido->titulo_prod ?></td>
                            <td><?php echo $pedido->detalhes_prod ?></td>
                            <td><?php echo $pedido->quantidade_prod ?></td>
                            <td>R$ <?php echo $pedido->total ?></td>
                            <td>
                            <?php 
                                if ($pedido->ativo == 1) { ?>
                                    <a data-toggle="modal" data-target="#ModalLongoExemplo">
                                    <i class="fas fa-check"></i></a>
                                <?php } ?>
                            </td>
                    </tr>
                        <?php 
                            }
                        ?>
                </tbody>
            </table>
        </div>
        <!-- MODAL -->
        <div class="modal fade" id="ModalLongoExemplo" tabindex="-1" role="dialog" aria-labelledby="TituloModalLongoExemplo" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TituloModalLongoExemplo">Lista de pedidos</h5>
                    <button type="btn btn-amber" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                   Esta encomenda já está sendo preparada.
                   Em caso de dúvidas, entre em contato pelo botão flutuante à direita da tela.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-amber" data-dismiss="modal">Fechar</button>
                </div>
                </div>
            </div>
            </div>
        </div>
</div>