<br><br><br><br><br><br>
<div class="container mt-5">
    <div class="row">
        <div class="fotoprod">
            <!-- Imagem -->
            <?php foreach ($produtos as $produto){?>
                <?php
                
                    if($produto->img == 1){
                        $fotoprod = base_url("assets/img/produto/".md5($produto->id).".jpg"); 
                }?>
                    <div class="view view-cascade overlay">
                        <img src="<?php echo $fotoprod ?>" class="card-img-top"/>
                    </div>
        </div>
            <div class="col-lg-4 mt-5 ml-2">
                <?php 
                    echo form_open('index.php/produto/add_produto_escolhido');
                ?>
                    <div class="row">
                        <div class="card card-cascade narrower card-ecommerce mb-5">
                        <div class="card-body card-body-cascade mt-3 mr-5">
                            <?php if($this->session->userdata('logado')){ ?>
                        
                                <!--Titulo-->
                                <h4 class="card-title mb-4 for">
                                    <input type="hidden" value="<?php echo $produto->id ?>" name="id">
                                    <input type="hidden" value="<?php echo $produto->titulo ?>" name="nome">
                                    <input type="hidden" value="<?php echo $usuario->nome ?>" name="userlogado">
                                    <input type="hidden" value="<?php echo $usuario->email ?>" name="email_user">
                                    <input type="hidden" value="<?php echo $usuario->telefone ?>" name="telefone_user">
                                    <label for="titulo_prod"><?php echo $produto->titulo ?>
                                    <input type="hidden" value="<?php echo $produto->titulo ?>" name="titulo_prod">
                                </h4>
                                <!-- Descrição -->
                                <label class="form" for="detalhes_prod"><?php echo $produto->detalhes ?>
                                <input type="hidden" value="<?php echo $produto->detalhes ?>" name="detalhes_prod">
                                <!-- Quantidade -->
                                <div class="px-1">
                                    <span class="float-left font-weight-bold for">
                                        <input type="hidden" value="<?php echo $produto->quantidade ?>" name="quantidade_prod"><br>
                                        <label class="price" for="preco_prod">R$<?php echo $produto->preco ?>
                                        <input type="hidden" value="<?php echo $produto->preco ?>" name="preco_prod"><br>                                       
                                    </span>
                                    <label for="quantidade">Digite a quantidade desejada:
                                        <input type="number" id="quantidade" required name="quantidade" class="mt-3 mb-1">
                                        <input type="submit" class="btn btn-amber" value="FAZER PEDIDO">                                        
                                </div>
                                <!-- P/ USUÁRIOS QUE NÃO ESTÃO LOGADOS! -->
                                <?php }else{ ?>
                                    <h4 class="card-title mb-4 for">
                                        <label for="titulo_prod"><?php echo $produto->titulo ?><br>
                                    </h4>
                                        <!-- Descrição -->
                                        <label class="form" for="detalhes_prod"><?php echo $produto->detalhes ?><br>
                                        <span class="float-left font-weight-bold for">
                                            <!-- Quantidade -->
                                            <label for="quantidade">Digite a quantidade desejada:
                                            <input type="number" id="quantidade" required name="quantidade" class="mt-3 mb-1">
                                            <!-- Preço -->
                                            <label class="mt-3" for="preco_prod">R$<?php echo $produto->preco ?>
                                        </span><br>
                                        <button class="btn btn-amber mt-4" href="<?= base_url('index.php/admin/login') ?>">FAZER PEDIDO</button>
                                <?php } ?>
                        </div>
                        </div> 
                    </div>
                <?php 
                    echo form_close();
                ?>
            </div>
            <?php 
            } 
            ?>
    </div>
</div>
