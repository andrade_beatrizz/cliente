<br><br><br>
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/mdb/css/style.css') ?>" media="screen" />
<br><br>
<div class="jumbotron p-0 ">
  <div class="view overlay rounded-top ">
    <img src="<?= base_url('assets/mdb/img/blog/carrossel.png') ?>" class="img-fluid">
    <a href="#">
      <div class="mask rgba-orange-slight"></div>
    </a>
  </div>
</div>
<div class="container">
<div class="text-center mt-5">
  <h3 class="card-title h3 my-4" id="sub"><strong>Últimas notícias</strong></h3>
</div><br><br><br>

 
    
      <div class="row col-md-11">
        <div class="col-md-4 offset-md-1 mx-3 my-3">
          <div class="view overlay">
            <img src="<?= base_url('assets/mdb/img/blog/hist.jpg')?>" height="290px" width="300px" alt="Sample image for first version of blog listing">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
        </div>
        <div class="col-md-7 text-md-left ml-3 mt-3">
          <a href="#!" class="orange-text">
            <h6 class="h6 pb-1" id="sub"><i class="fab fa-gratipay"></i> Mini Blog</h6>
          </a>
          <h4 class="h4 mb-4" id="sub">História do Artesanato</h4>
          <p class="font-weight-normal">A história do artesanato tem início no mundo com a própria história do homem,
            pois a necessidade de se produzir bens de utilidades e uso rotineiro,
            e até mesmo adornos, expressou a capacidade criativa e produtiva como forma de trabalho...</p>
          
          <hr>
        </div>
        <div class="col-md-4 offset-md-1 mx-3 my-3">
          <div class="view overlay">
            <img src="<?= base_url('assets/mdb/img/blog/chaveiros.jpg')?>" height="270px" width="300px" alt="Sample image for first version of blog listing">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
        </div>
        <div class="col-md-7 text-md-left ml-3 mt-3">
          <a href="#!" class="orange-text">
            <h6 class="h6 pb-1" id="sub"><i class="fab fa-gratipay"></i> Mini Blog</h6>
          </a>
          <h4 class="h4 mb-4"id="sub">Nossos produtos</h4>
          <p class="font-weight-normal">Agendas, bolsas, decoração, madeiras, chaveiros, canecas e diversos outros produtos para
            todo tipo de gosto. Conheça os nossos produtos!</p>
        
          <hr>
        </div>
        <div class="col-md-4 offset-md-1 mx-3 my-3">
          <div class="view overlay">
            <img src="<?= base_url('assets/mdb/img/blog/natal.jpg')?>" height="270px" width="300px" alt="Sample image for first version of blog listing">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
        </div>
        <div class="col-md-7 text-md-left ml-3 mt-3">
          <a href="#!" class="orange-text">
            <h6 class="h6 pb-1" id="sub"><i class="fab fa-gratipay"></i> Mini Blog</h6>
          </a>
          <h4 class="h4 mb-4" id="sub">Promoções!</h4>
          <p class="font-weight-normal">Nosso objetivo é ferecer cada vez mais
            produtos de qualidade e preço justo para todo tipo de público. Clique aqui para
            ver as nossas promoções!</p>
        
          <hr>
        </div>
        <div class="col-md-4 offset-md-1 mx-3 my-3">
          <div class="view overlay">
            <img src="<?= base_url('assets/mdb/img/about/miss.jpg')?>" height="270px" width="300px" alt="Sample image for first version of blog listing">
            <a>
              <div class="mask rgba-white-slight"></div>
            </a>
          </div>
        </div>
        <div class="col-md-7 text-md-left ml-3 mt-3">
          <a href="#!" class="orange-text">
            <h6 class="h6 pb-1" id="sub"><i class="fab fa-gratipay"></i> Mini Blog</h6>
          </a>
          <h4 class="h4 mb-4"id="sub">Seja bem-vindo</h4>
          <p class="font-weight-normal">Aqui a garantia não é somente de qualidade. É também
            de muita competência, respeito, comprometimento e muito amor!</p>

        </div>
      </div>
  </div>
</div>
</div><br>
<div class="container">
  <h3 class="card-title h3 my-4 text-center" id="sub"><strong>Galeria</strong></h3><br>
  <div class="row">
    <div class="col-md-3">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/bon1.jpg')?>" height="250" width="300px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/carrossel2.jpg')?>" height="250" width="300px">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/carrossel7.jpg')?>" height="250" width="300px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/carrossel4.jpg')?>" height="250" width="300px">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/item1.jpg')?>" height="250" width="300px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/item2.jpg')?>" height="250" width="300px">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?=base_url('assets/mdb/img/blog/carrossel9.jpg')?>" height="250" width="300px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?=base_url('assets/mdb/img/blog/bon11.jpg')?>" height="250" width="300px">
          </div>
        </div>
      </div>
    </div>
  </div><br><br><br>
  <div class="row">
    <div class="col-md-3">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?=base_url('assets/mdb/img/blog/bon16.jpg')?>" height="250" width="300px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/bon21.jpg')?>" height="250" width="300px">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/bon12.jpg')?>" height="250" width="300px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/enf17.jpg')?>" height="250" width="300px">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/enf25.jpg')?>" height="250" width="300px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/enf28.jpg')?>" height="250" width="300px">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-3">
      <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/mad15.jpg')?>" height="250" width="300px">
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" src="<?= base_url('assets/mdb/img/blog/mad13.jpg')?>" height="250" width="300px">
          </div>
        </div>
      </div>
    </div>
    </div>
  </div><br><br>
