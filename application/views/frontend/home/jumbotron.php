<div class="container">    
        <div class="jumbotron text-center">
            <div id="subjumb">
                <h4 class="card-title h4 pb-1"><strong>Venha nos Conhecer</strong></h4>
            </div>

            <div class="view overlay my-4">
                <img src="<?= base_url('assets/mdb/img/home/ban.jpg')?>" class="img-fluid" alt=""> 
                <a href="<?= base_url('index.php/sobrenos');?>">
                <div class="mask rgba-white-slight"></div>
                </a>
            </div>

            <div id="subjumb">
                <h5 class="h5 mb-4">Estamos também nas Redes Sociais!</h5>
            </div>

            <a href="https://www.instagram.com/viane_galdino/" target="_blank"><img src="<?= base_url('assets/mdb/img/icons/insta.png')?>" width="40" height="30"></a>
            <a href="https://www.facebook.com/viviane.galdinodasilva" target="_blank"><img src="<?= base_url('assets/mdb/img/icons/face.png')?>" width="50" height="45"></a>
        </div>
</div>