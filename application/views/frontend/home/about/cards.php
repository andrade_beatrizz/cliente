<link rel="stylesheet" type="text/css" href="<?=base_url('assets/mdb/css/style.css') ?>" media="screen" />
    <div class="text-center mt-5">
        <div id="sub"><h3><p class="font-weight-bold"> SOBRE NÓS </p></h3></div>
    </div>

    <div id="featured">
		<div class="container">
			<div class="row">
					<!-- MISSÃO -->
					<div class="col">
						<div class="card-deck">
							<div class="card mb-4">
								<div class="view overlay">
									<img class="card-img-top" src="<?=base_url('assets/mdb/img/about/miss.jpg') ?>" alt="Card image cap">
									<a href="#!">
									<div class="mask rgba-white-slight"></div>
									</a>
								</div>
								<div class="card-body">
									<div id="sub"><h4 class="card-title">Missão</h4></div>
									<p class="card-text">Fazer artesanatos procurando
														a perfeição e qualidade, para ter uma satisfação e um retorno positivo dos clientes.</p>
								</div>
							</div>
						</div>
					</div>

					<!-- VISÃO -->
					<div class="col">
						<div class="card-deck">
							<div class="card mb-4">
								<div class="view overlay">
									<img class="card-img-top" src="<?=base_url('assets/mdb/img/about/vis.jpg') ?>" alt="Card image cap">
									<a href="#!">
									<div class="mask rgba-white-slight"></div>
									</a>
								</div>
								<div class="card-body">
									<div id="sub"><h4 class="card-title">Visão</h4></div>
									<p class="card-text">Ser referência no mercado de artesanato, oferecer cada vez mais produtos de qualidade e preço justo para todo tipo de público.</p>
								</div>
							</div>
						</div>
					</div>

					<!-- VALORES -->
					<div class="col">
						<div class="card-deck">
							<div class="card mb-4">
								<div class="view overlay">
									<img class="card-img-top" src="<?=base_url('assets/mdb/img/about/val.jpg') ?>" alt="Card image cap">
									<a href="#!">
									<div class="mask rgba-white-slight"></div>
									</a>
								</div>
								<div class="card-body">
									<div id="sub"><h4 class="card-title">Valores</h4></div>
									<p class="card-text"> Ética, Competência, Respeito, Comprometimento, Flexibilidade, Delicadeza e Muito Amor!</p>
								</div>
							</div>
						</div>
					</div>

			</div>
		</div>
	</div>
