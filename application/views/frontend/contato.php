<div class="container mt-5"><br><br><br><br>
<style type="text/css">
    body{
        background-image: url(<?=base_url('assets/mdb/img/about/cont.jpg') ?>);
        background-repeat: no-repeat;

    }
</style>
    <h1 class="categorias mt-5" id="sub"> 
        <?php echo $subtitulo ?>
    </h1>
        <div class="row">
            <div class="col-md-6 mb-5">
                <!-- Início do Form -->
                <?=
                validation_errors('<div class="alert alert-danger">', '</div>');
                ?>

                    <form method="POST" class="mt-3">
                        <div class="form-group">
                            <label id="sub" class="form text-black">Nome do usuário</label>
                                <input id="sub" class="form-control" placeholder="Usuário" name="mensagens[nome]" type="text" 
                                value="<?php echo set_value('mensagens[nome]')?>">
                        </div>
                        <div class="form-group">
                            <label id="sub" class="form text-black ">E-mail</label>
                                <input id="sub" class="form-control" placeholder="E-mail" name="mensagens[email]" type="email"
                                value="<?php echo set_value('mensagens[email]')?>">
                        </div>
                        <div class="form-group">
                            <label id="sub" class="form text-black ">Telefone</label>
                                <input id="sub" class="form-control" placeholder="(DD)X-XXXXXXXX" name="mensagens[telefone]" type="number"
                                value="<?php echo set_value('mensagens[telefone]')?>">
                        </div>
                        <div class="form-group">
                            <label id="sub" class="form text-black ">Mensagem</label>
                                <textarea id="sub" class="form-control" placeholder="Mensagem" name="mensagens[mensagem]" type="text"></textarea>
                        </div>
                        
                            <button class="btn btn-lg btn-warning btn-block form">Entrar</button>
                            
                        </div>
                    </form>
                     <!--/Fim do formulário-->
                     <!--Início inform\ções -->
                        <div class="col-md-3 text-center mt-5 ml-5">
                            <ul class="list-unstyled mb-0">
                                <li><a href="https://goo.gl/maps/9t3G4t5iyz4QCX2H9" target="_blank"><i class="fas fa-map-marker-alt fa-2x mb-4"></a></i>
                                    <p class="mb-5 for" id="sub">R. São Mateus do Sul <br>n° 229 <br> Jd. Santa Vicência</p>
                                </li>

                                <li><i class="fas fa-phone mt-4 fa-2x mb-4"></i>
                                    <p class="mb-5 for"  id="sub">+55 11 975584376</p>
                                </li>

                                <li><i class="fas fa-envelope mt-4 fa-2x mb-4"></i>
                                    <p class="mb-5 for" id="sub">atelieannyesantos@outlook.com</p>
                                </li>
                            </ul>
                        </div>
                    <!--/Fim informações--> 
            </div> 
        </div>
</div>

