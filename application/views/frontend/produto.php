<div class="container mt-3"><br><br>
  <section class="text-center my-5">

      <h2 class="categorias mb-5">
        <?php 
        foreach($produtos as $produto)
        ?>
      </h2>

    <div class="row">
    <?php foreach ($produtos as $produto){?>
      <div class="col-lg-3 col-md-6 mb-lg-0 mb-4 mt-4">
        <div class="card card-cascade narrower card-ecommerce">
          <div class="view view-cascade overlay">
            <?php
              if($produto->img == 1){
                $fotoprod = base_url("assets/img/produto/".md5($produto->id).".jpg"); 
            ?>
              <img src="<?php echo $fotoprod ?>"
              class="card-img-top" alt="sample photo">
            <?php
              }
            ?>
          </div>

          <div class="card-body card-body-cascade text-center">
              <h4 class="card-title">
                <strong>
                  <p class="titulo"><?php echo $produto->titulo ?><a href="<?= base_url('index.php/produtos/'.$produto->id)?>"
                  ></a></p>
                </strong>
              </h4>
            <p class="detalhes"><?php echo $produto->detalhes ?></p>
              <div class="card-footer px-1">
                  <strong><?php echo 'R$'.$produto->preco ?></strong>
              </div>
              <a class="btn bot" href="<?php echo base_url('index.php/produto/'.$produto->id.'/'.limpar($produto->titulo))?>">
                  Visualizar</a>
          </div>
        </div>
      </div>
      <!-- Fim do card. -->
    <?php 
    }
    ?>
    </section>
  <?php echo "<div class='paginacao'>".$links_paginacao."</div>" ?>
</div>
<hr>
   