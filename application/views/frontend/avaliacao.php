<div class="container">
    <!-- AVALIAÇÃO P/ USUÁRIOS LOGADOS -->
    <h2 class="text-center" id="sub"> AVALIAÇÕES </h2>
        <?php if($this->session->userdata('logado')){ ?>
            <h5 class="mt-4" id="sub"> Conte-nos sua experiência com este produto,  
                <?php echo $this->session->userdata('userlogado')->nome; ?>!
            </h5>
            <?php foreach ($produtos as $produto){?>
                <?=
                    validation_errors('<div class="alert alert-danger">', '</div>');
                ?>
                    <form method="POST" class="mt-3">
                        <div class="form-group">
                            <input type="hidden" value="<?php echo $produto->titulo ?>" name="avaliacao_produto[nome]">
                            <input type="hidden" value="<?php echo $produto->id ?>" name="avaliacao_produto[id_produto]">
                            <input type="hidden" value="<?php echo $usuario->nome ?>" name="avaliacao_produto[user]">
                            <textarea id="sub" class="form-control" placeholder="Sua avaliação" name="avaliacao_produto[comentario]" type="text"></textarea>
                            <input id="sub" type="submit" class="btn btn-amber mb-5" value="Enviar">             
                        </div>
                    </form>
        <?php } 
        ?>

    <!-- AVALIAÇÃO P/ USUÁRIOS DESLOGADOS -->
        <?php }else{ ?>
            <h5 class="mt-4" id="sub">
                 Faça <a class="text-warning" href="<?= base_url('index.php/admin/login') ?>">login</a> para nos contar sua experiência com este produto!
            </h5><br>
            <hr>
        <?php } ?>
</div>
